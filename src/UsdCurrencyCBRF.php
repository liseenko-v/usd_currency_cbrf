<?php

namespace Lvv\UsdCurrencyCBRF;

class UsdCurrencyCBRF {
    public function getCurrency($date) {

        try {
            //input date format d.m.Y, parse date format d/m/Y
            $date_parse = str_replace('.', '/', $date);            

            $url_cbrf = "http://www.cbr.ru/scripts/XML_dynamic.asp?date_req1=".$date_parse."&date_req2=".$date_parse."&VAL_NM_RQ=R01235";
            $xml_cbrf = simplexml_load_file($url_cbrf);
            if ($xml_cbrf != null) {		
                if (isset($xml_cbrf->Record->Value) && $xml_cbrf->Record->Value != null) {
                    $currency_usd_cbrf = floatval(str_replace(',', '.', $xml_cbrf->Record->Value));				
                    return $currency_usd_cbrf;
                } else {
                    return false;
                }			
            } else {
                return false;
            }
        }
        catch(Exception $exc) {
            return false;
        }
		
    }
} 